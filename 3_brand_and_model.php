<?php
include('0_header.html');
include('0_dati.php');
?>
<h1>Fai la tua scelta</h1>
<form action="1_risultato.php" class="form-inline">
    <div class="form-group">
        <label for="brand">Marca:</label>
        <select name="brand" id="brand" class="form-control">
            <option value='0'>---</option>
            <?php
            foreach ($brand as $key => $value) {
                echo "<option value='$key'>$value</option>";
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="model">Modello:</label>
        <select name="model" id="model" class="form-control">
            <option value='0'>---</option>
        </select>
    </div>

    <input type="submit" value="Prosegui" class="btn btn-success">
</form>
<script>
    $('#brand').change(function () {
        console.log($('#brand').val())
        $.ajax({
            url: "3_json_model.php",
            data: {'brand': $('#brand').val()},
            dataType: "json"
        }).done(function (result) {
            var $select = $('#model');
            $select.find('option').remove();
            $.each(result, function(key, value) {
                $('<option>').val(key).text(value).appendTo($select);
            });
        });
    });

</script>

<?php
include('0_footer.html');
?>

