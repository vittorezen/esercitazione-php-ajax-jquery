<?php

$brand = [
    'fiat' => 'Fiat',
    'opel' => 'Opel',
    'renault' => 'Renault'
];
$model['fiat'] = [
    '500' => '500',
    '500xl' => '500xl',
    'punto' => 'punto'
];
$model['opel'] = [
    'meriva' => 'Meriva',
    'astra' => 'Astra'
];
$model['renault'] = [
    'twingo' => 'Twingo',
    'scenic' => 'Scenic',
    'megane' => 'Megane'
];