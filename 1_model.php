<?php
include('0_header.html');
include('0_dati.php');
?>
<h1>Fai la tua scelta</h1>
<form action="1_risultato.php" class="form-inline">
    <input type="hidden" name="brand" id="brand" value="<?php echo $_REQUEST['brand'] ?>">
    <div class="form-group">
        <label for="model">Modello:</label>
        <select name="model" id="model" class="form-control">
            <?php
            foreach ($model[$_REQUEST['brand']] as $key => $value) {
                echo "<option value='$key'>$value</option>";
            }
            ?>
        </select>
    </div>
    <input type="submit" value="Prosegui" class="btn btn-success">
</form>
<?php
include('0_footer.html');
?>

